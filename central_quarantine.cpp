#include "central_quarantine.h"
#include <condition_variable>
#include <cstring>
#include "jemalloc/jemalloc.h"
#include "malloc.h"
#include "memory_ranges.h"
#include "minesweeper_config.h"
#include <mutex>
#include "quarantine.h"
#include "stop_world.h"
#include <thread>

static inline bool should_sweep(size_t total,
                                size_t quarantined,
                                size_t unmapped,
                                size_t failed,
                                size_t unmapped_failed) {
    // Do not count memory that failed to be deallocated -- likely to fail again for a while
    if (total > (failed - unmapped_failed))
        total -= failed - unmapped_failed;

    quarantined -= (unmapped + failed - unmapped_failed);

    size_t denom = total / 100;

    bool sweep_threshold_hit =
            (quarantined >= minesweeper::SWEEP_THRESHOLD * denom);
    bool unmapped_threshold_hit =
            (unmapped >= minesweeper::UNMAPPED_THRESHOLD * denom);

    return sweep_threshold_hit || unmapped_threshold_hit;
}

static inline bool backstop(size_t total,
                            size_t quarantined,
                            size_t unmapped,
                            size_t failed,
                            size_t unmapped_failed) {
    // Do not count memory that failed to be deallocated -- likely to fail again for a while
    if (total > (failed - unmapped_failed))
        total -= failed - unmapped_failed;

    quarantined -= (unmapped + failed - unmapped_failed);

    return (quarantined >= minesweeper::BACKSTOP_THRESHOLD * total / 100);
} 

void CentralQuarantine::moveAllFrom(ngc_vector<Item>& vect) {
    // Do unmapping and collect stats
    size_t new_size = 0;
    size_t new_unmapped = 0;
    for (const Item& item : vect) {
        new_size += item.size();
        // Unmap parts covering whole pages
        new_unmapped += item.unmap_and_zero();
    }

    size_t quarantined, unmapped, failed, unmapped_failed, last_failed, last_unmapped_failed;
    {WITH_LOCK(items_mtx)
        new_items.add_all(vect);
        quarantined = (quarantined_mem += new_size);
        unmapped = (unmapped_mem += new_unmapped);
        failed = failed_frees;
        unmapped_failed = unmapped_failed_frees;
        last_failed = last_failed_frees;
        last_unmapped_failed = last_unmapped_failed_frees;
    }

    // Empty vect and save it for swapping next time
    vect.erase_all();

    size_t total = memory_ranges().totalMemory();
    if (should_sweep(total, quarantined, unmapped, failed, unmapped_failed)) {
        if (!minesweeper::CONCURRENT) {
            central_quarantine.sweep();
            return;
        }

//      bool backstopped = false;

        // Test-and-test-and-wait while backstop condition holds
        int i=0;
        if (backstop(total, quarantined, unmapped, last_failed, last_unmapped_failed)) {
            {std::unique_lock<std::mutex> lock(items_mtx);
                while (true) {
                    quarantined = quarantined_mem;
                    unmapped = unmapped_mem;
                    failed = last_failed_frees;
                    unmapped_failed = last_unmapped_failed_frees;

                    total = memory_ranges().totalMemory();

                    if (!backstop(total, quarantined, unmapped, failed, unmapped_failed))
                        break;
        //          if (!backstopped) msweeper_dlog("backstop hit\n");
        //          backstopped = true;

                    signalSweep();
                    if (i++ < 10)  {
                        pthread_yield();
                    } else {
                        backstop_cond.wait(lock);
                    }
                }
            }
        } else signalSweep();

//      if (backstopped) msweeper_dlog("resumed after backstop\n");
    }
}

static constexpr int DEBUG_SAMPLE_COUNT = 0;
struct DbgSample {
    void *begin, *end;
    int idx;
    void** ptr;
    int cnt;

    void reg(void** p, void* q) {
        ASSERT((q >= begin) && (q < end));
        ptr = p;
        cnt++;
    }

    void set(void *_begin, void *_end, int _idx) {
        begin = _begin;
        end = _end;
        idx = _idx;
        ptr = 0;
        cnt = 0;
    }

    bool operator<(const DbgSample& oth) const {
        return (begin < oth.begin);
    }

    static void mark(DbgSample* samples, void** p, void* q) {
        int i = 0, j = DEBUG_SAMPLE_COUNT-1;
        while (i <= j) {
            int k = (i + j) / 2;
            if (q < samples[k].begin) {
                j = k-1;
            } else if (q < samples[k].end) {
                samples[k].reg(p, q);
                for (int l=k-1; (l>=0) && (q < samples[l].end); l--)
                    samples[l].reg(p, q);
                for (int l=k+1; (l<DEBUG_SAMPLE_COUNT) && (q >= samples[l].begin); l++)
                    samples[l].reg(p, q);
                return;
            } else i = k+1;
        }
    }

    static void check(DbgSample* samples, BaseQuarantine::Item item, bool marked) {
        int i = 0, j = DEBUG_SAMPLE_COUNT-1;
        while (i <= j) {
            int k = (i + j) / 2;
            if (item.end_ptr <= samples[k].begin) {
                j = k-1;
            } else if (samples[k].end <= item.start_ptr) {
                i = k+1;
            } else {
                ASSERT((item.start_ptr == samples[k].begin) && (item.end_ptr == samples[k].end));
                if (marked) {
                    ASSERT(samples[k].ptr);
                } else {
                    ASSERT(!samples[k].ptr);
                }
                fprintf(stderr, ".");
                return;
            }
        }
    }

    static void dump(DbgSample* samples) {
        for (int i=0; i<DEBUG_SAMPLE_COUNT; i++) {
            if (samples[i].ptr) {
                int still = ((*samples[i].ptr >= samples[i].begin) && (*samples[i].ptr < samples[i].end));
                fprintf(stderr, "%p->%p: %p (%d) %d\n",
                        samples[i].begin, samples[i].end, samples[i].ptr, samples[i].cnt, still);
            }
        } 
    }
};

static DbgSample dbg_samples[DEBUG_SAMPLE_COUNT];

size_t sweep_count = 0;

static void* min_entry;
static void* max_entry;

static void mark_range(void *start, size_t size) {
    void **end = (void**)add_bytes(start, size);

    for (void **p = (void**)start; p < end; p++) {
        void *q = *p;
        if (DEBUG_SAMPLE_COUNT)
            DbgSample::mark(dbg_samples, p, q);
        central_quarantine.markItemsContaining(q, min_entry, max_entry);
    }
}

void CentralQuarantine::sweep() {
    sweep_count++;
    msweeper_dlog("sweep()\n");

    size_t quarantined_at_start, unmapped_at_start;

    {WITH_LOCK(items_mtx)
        items.add_all(new_items);
        new_items.erase_all();
        last_failed_frees = failed_frees;
        last_unmapped_failed_frees = unmapped_failed_frees;
        failed_frees = quarantined_at_start = quarantined_mem;
        unmapped_failed_frees = unmapped_at_start = unmapped_mem;
        released_mem = released_unmapped = 0;
    }

    msweeper_dlog_val("Quarantined at start: ", quarantined_at_start);

    if (DEBUG_SAMPLE_COUNT > 0) {
        for (int i=0; i<DEBUG_SAMPLE_COUNT; i++) {
            int j = rand() % items.size();
            if ((i < 500) && (items.size() > 10000))
                j = rand() % 10000;
            dbg_samples[i].set(items[j].start_ptr, items[j].end_ptr, j);
        }
        std::sort(dbg_samples, dbg_samples+DEBUG_SAMPLE_COUNT);
    }

    if (minesweeper::STOP_WORLD_RESWEEP)
        stop_the_world().protectHeap();

    MemoryRanges &mr = memory_ranges();

    void *heap_start, *heap_end;
    mr.getHeap(heap_start, heap_end);
    size_t heap_size = size_bytes(heap_start, heap_end);
    min_entry = heap_start;
    max_entry = heap_end;
    // For each memory word, mark quarantined items pointed at.
    if (minesweeper::NUM_SWEEPERS) {
        helpers_remaining.store(minesweeper::NUM_SWEEPERS,
                                std::memory_order_relaxed);

        size_t seg_size = (size_t)page_aligned_up((void*)
                                      (heap_size / minesweeper::NUM_SWEEPERS));

        size_t i;
        for (i = 0; i < minesweeper::NUM_SWEEPERS-1; i++)
            helper_ctl[i].signal(add_bytes(heap_start,     i * seg_size),
                                 add_bytes(heap_start, (i+1) * seg_size),
                                 true);
        helper_ctl[i].signal(add_bytes(heap_start, i * seg_size), heap_end, true);
    } else mr.apply(mark_range, 0, heap_start, heap_end);

    mr.apply(mark_range, 0); // Sweep stacks and globals

    if (minesweeper::NUM_SWEEPERS) {
        {std::unique_lock<std::mutex> lock(helper_done_mtx);
            while (!terminating && helpers_remaining.load(std::memory_order_relaxed))
                helper_done_cond.wait(lock);
            if (terminating)
                pthread_exit(NULL);
        }
    }

    if (minesweeper::STOP_WORLD_RESWEEP) {
        stop_the_world().stop();

        if (terminating) {
          stop_the_world().resume();
          pthread_exit(NULL);
        }

        mr.getHeap(heap_start, heap_end);
        heap_size = size_bytes(heap_start, heap_end);

        if (minesweeper::NUM_SWEEPERS
          /*&& heap_size > minesweeper::PARALLEL_MIN_HEAP_SIZE*/) {
            helpers_remaining.store(minesweeper::NUM_SWEEPERS,
                                    std::memory_order_relaxed);

            size_t seg_size = (size_t)page_aligned_up((void*)
                                          (heap_size / minesweeper::NUM_SWEEPERS));

            size_t i;
            for (i = 0; i < minesweeper::NUM_SWEEPERS-1; i++)
                helper_ctl[i].signal(add_bytes(heap_start,     i * seg_size),
                                     add_bytes(heap_start, (i+1) * seg_size));
            helper_ctl[i].signal(add_bytes(heap_start, i * seg_size), heap_end);
        } else mr.applyDirtyNoLock(mark_range, heap_start, heap_end);
            
        mr.applyDirtyNoLock(mark_range, 0); // Sweep stacks and globals
        if (minesweeper::NUM_SWEEPERS) {
            {std::unique_lock<std::mutex> lock(helper_done_mtx);
                while (helpers_remaining.load(std::memory_order_relaxed))
                    helper_done_cond.wait(lock);
            }
        }

        stop_the_world().resume();
    }

    if (DEBUG_SAMPLE_COUNT)
        DbgSample::dump(dbg_samples);

    if (minesweeper::NUM_SWEEPERS && items.size() > minesweeper::PARALLEL_DEALLOCATE_THRESHOLD) {
        // Signal helpers to deallocate in chunks of seg_size
        helpers_remaining.store(minesweeper::NUM_SWEEPERS, std::memory_order_relaxed);
        constexpr size_t items_per_page = 64 / sizeof(Item); static_assert(isPo2(items_per_page));

        size_t seg_size = (size_t)aligned_up((void*)(items.size() / minesweeper::NUM_SWEEPERS),
                                             items_per_page);
        size_t i;
        for (i = 0; i < minesweeper::NUM_SWEEPERS-1; i++)
            helper_ctl[i].signal(i * seg_size, (i+1) * seg_size);
        helper_ctl[i].signal(i * seg_size, items.size());

        // Wait for helpers
        {std::unique_lock<std::mutex> lock(helper_done_mtx);
            while (helpers_remaining.load(std::memory_order_relaxed))
                helper_done_cond.wait(lock);
        }
    } else {
        // No helpers, do it in-thread
        deallocateUnmarked(0, items.size());
    }

    // Clear the marks we have made
    recyclingShadow.clear();
    shadow.clear();

    uint64_t ffrees, qmem;

    {WITH_LOCK(items_mtx)
        ASSERT(quarantined_at_start >= released_mem);
        ASSERT(unmapped_at_start >= released_unmapped);
        ffrees = last_failed_frees = failed_frees = quarantined_at_start - released_mem;
        last_unmapped_failed_frees = unmapped_failed_frees = unmapped_at_start - released_unmapped;
        qmem = quarantined_mem -= quarantined_at_start - failed_frees;
        unmapped_mem -= unmapped_at_start - unmapped_failed_frees;

        backstop_cond.notify_all();
    }

    msweeper_dlog_val("Total mem:            ", memory_ranges().totalMemory());
    msweeper_dlog_val("Quarantined at end:   ", qmem);
    msweeper_dlog_val("Failed to free:       ", ffrees);
    msweeper_dlog("sweep() done.\n");

    purge_all();
    msweeper_dlog("purge_all() done\n");

    size_t i = 0;
    for (Item& item : items) {
        if (!item.start_ptr) continue;
        items[i++] = item;
    }
    items.pop_back(items.size() - i);
}

void CentralQuarantine::deallocateUnmarked(size_t start, size_t finish) {
    size_t removed_size = 0, removed_unmapped = 0;

    for (size_t i=start; i<finish; i++) {
        if (i+32 < finish)
            _mm_prefetch((void*)shadow.entry(items[i+32].start_ptr), _MM_HINT_T0);

        bool marked = shadow.marked(items[i].start_ptr, items[i].size());
        if (DEBUG_SAMPLE_COUNT)
          DbgSample::check(dbg_samples, items[i], marked);

        // Remove from quarantine if unmarked
        if (!marked) {
            // Update stats
            removed_size += items[i].size();
            removed_unmapped += items[i].remap();

            // Check if this allocation has been recycled already (double-free)
            bool already_freed = recyclingShadow.mark(items[i].start_ptr);

            if (already_freed) {
                // If in debug mode, report double free event. Otherwise,
                // silently and safely ignore (double free is still eliminated!)
                msweeper_dlog("double free detected!\n");
            } else {
                // Deallocate corresponding memory allocation
                je_sdallocx(items[i].start_ptr, items[i].size(), 0);
            }

            items[i].start_ptr = items[i].end_ptr = NULL;
        }
    }

    // Update stats
    {WITH_LOCK(items_mtx)
        released_mem += removed_size;
        released_unmapped += removed_unmapped;
    }

    // Return allocations from thread-local cache to the arenas they came from
    flush_tcache();
}

// Entry point for sweeper thread
void CentralQuarantine::sweep_thread_main() {
    // Read info from /proc/self/maps
    registerMapRanges();

    while (true) { // Until termination
        {std::unique_lock<std::mutex> need_sweep_lock(sweeper_mtx);
            while (!terminating && !need_sweep_signalled.load(std::memory_order_acquire))
                sweeper_cond.wait(need_sweep_lock);

            if (terminating) return;

            need_sweep_signalled.store(false, std::memory_order_relaxed);
        }

        size_t quarantined, unmapped, failed, unmapped_failed;
        {WITH_LOCK(items_mtx)
            quarantined = quarantined_mem;
            unmapped = unmapped_mem;
            failed = failed_frees;
            unmapped_failed = unmapped_failed_frees;
        }

        size_t total = memory_ranges().totalMemory();
        if (should_sweep(total, quarantined, unmapped, failed, unmapped_failed))
            central_quarantine.sweep();
    }
}

void CentralQuarantine::signalSweep() {
    if (need_sweep_signalled.load(std::memory_order_relaxed)) return;
    {WITH_LOCK(sweeper_mtx)
        need_sweep_signalled.store(true, std::memory_order_release);
        sweeper_cond.notify_one();
    }
}

void CentralQuarantine::sweep_helper_t::do_sweep(CentralQuarantine *parent,
                            void* start_addr, void* end_addr, bool use_locking) {
    if (use_locking) {
        memory_ranges().apply(mark_range, id, start_addr, end_addr);
    } else {
        memory_ranges().applyDirtyNoLock(mark_range, start_addr, end_addr);
        memory_ranges().applyDirtyNoLock(mark_range, id);
        stop_the_world().invalidateBuffer();
    }
}

void CentralQuarantine::sweep_helper_t::signalReady(CentralQuarantine *parent) {
    unsigned int r = parent->helpers_remaining
                          .fetch_sub(1, std::memory_order_acq_rel);

    if (r==1) {
        {WITH_LOCK(parent->helper_done_mtx) }
        parent->helper_done_cond.notify_one();
    }
}

void CentralQuarantine::sweep_helper_t::main(CentralQuarantine *parent) {
    while (true) {
        {std::unique_lock<std::mutex> lock(mtx);
            while (!parent->terminating && !start && (start == end))
                cond.wait(lock);
            if (parent->terminating) return;
        }

        if (deallocating) {
            parent->deallocateUnmarked((uint64_t)start, (uint64_t)end);
        } else {
            do_sweep(parent, (void*)start, (void*)end, use_locking);
        }

        start = end = 0;
        signalReady(parent);
    }
}

void CentralQuarantine::sweep_helper_t::signal(uint64_t start_ind, uint64_t end_ind) {
    {WITH_LOCK(mtx)
        start = (uintptr_t)start_ind;
        end = (uintptr_t)end_ind;
        use_locking = false;
        deallocating = true;
    }

    cond.notify_one();
}

void CentralQuarantine::sweep_helper_t::signal(void *start_addr, void *end_addr, bool locking) {
    {WITH_LOCK(mtx)
        start = (uintptr_t)start_addr;
        end = (uintptr_t)end_addr;
        use_locking = locking;
        deallocating = false;
    }

    cond.notify_one();
}

