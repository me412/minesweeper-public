#ifndef __MEMORY_RANGES_H_INCLUDED_
#define __MEMORY_RANGES_H_INCLUDED_

#include <algorithm>
#include <atomic>
#include <condition_variable>
#include "helper_defs.h"
#include <iterator>
#include "linked_list.h"
#include "minesweeper_config.h"
#include <mutex>
#include <optional>
#include "shadow.h"
#include <sys/mman.h>
#include <thread>

const static int mmap_flags = MAP_ANONYMOUS | MAP_PRIVATE;

inline void UNPROTECT(void *addr, size_t size)
{ ASSERT(!mprotect(addr, size, PROT_READ | PROT_WRITE)); }

inline void PROTECT(void *addr, size_t size)
{ // Discard (zero) and protect away pages, but keep the range
  ASSERT(mmap(addr, size, PROT_NONE, mmap_flags | MAP_FIXED, -1, 0) == addr);
}

struct StackMemoryRange;

/** Keeps track of memory ranges registered by extent hooks from jemalloc. */
class MemoryRanges {
    std::mutex mtx;

    struct aligned_atomic_ptr_t { alignas(64) std::atomic<void*> x; };
    aligned_atomic_ptr_t sweepBlocks[minesweeper::NUM_SWEEPERS+1];

    static constexpr size_t SweepBlockSize = 8 * PAGE_SIZE;

    PageShadow unmappedPages;

    static constexpr size_t MapRangeMaxCount = 32;
    size_t mapRangeCount = 0;
    void* mapRangeStarts[MapRangeMaxCount];
    size_t mapRangeSizes[MapRangeMaxCount];

    std::atomic<size_t> unmappedMem;
  public:
    static constexpr size_t StackMaxCount = 64;
  private:
    StackMemoryRange *stacks[StackMaxCount + 1];
    size_t stackCount = 0;
  public:

    bool launching_sweeper = false;

    // Register modification (thread-safe). On return, modification is safe to perform.
    void unmapRange(void *addr, size_t size);
    void remapRange(void *addr, size_t size);

    void addMapRange(void *start, void *end);
    int addStack(StackMemoryRange *stack);
    void removeStack(int stack_id);

    using apply_fn = void (*)(void *addr, size_t size);
    void apply(apply_fn fn, int sweeper_id);
    void apply(apply_fn fn, int sweeper_id, void *start, void *end);
    void applyDirtyNoLock(apply_fn fn, void *start, void *end);
    void applyDirtyNoLock(apply_fn fn, int id);

    inline bool isUnmapped(void *p) { return unmappedPages.marked(p); }

    void lock() { mtx.lock(); }
    void unlock() { mtx.unlock(); }

    std::atomic<void*> heapPtr;
    size_t totalMemory()
        { return size_bytes(heapPtr.ST_LOAD, sbrk(0)) - unmappedMem.ST_LOAD; }

    void getHeap(void *&begin, void *&end) {
        begin = heapPtr;
        end = sbrk(0);
    }
};

void registerMapRanges();

MemoryRanges& memory_ranges();

inline void unmap_mem_range(void* start_ptr, size_t size) {
    memory_ranges().unmapRange(start_ptr, size);
    PROTECT(start_ptr, size);
}
inline void remap_mem_range(void* start_ptr, size_t size) {
    UNPROTECT(start_ptr, size);
    memory_ranges().remapRange(start_ptr, size);
}

struct StackMemoryRange {
    void *stackBottom; // Bottom (max address) of the stack. This is constant.
    std::atomic<void*> stackTop = (void*)-1; // Top (min address), this may be out-of-date; should be over-estimate (of allocated range)
    int stack_id = MemoryRanges::StackMaxCount;

    __attribute__((noinline))
    void refreshStackTop() {
        // The min address (top) of the stack changes with every call/return.
        // Estimate the stack pointer (top of stack) using a dummy variable in our stack frame.
        // Allocated pages remain valid, so take minimum of all estimates.
        uint64_t dummy_variable = 0;
        dummy_variable++;
        void *top = page_aligned((void*)&dummy_variable);
        if (top < stackTop.load(std::memory_order_relaxed)) {
            stackTop.store(top, std::memory_order_relaxed);
        }
    }

    inline void registerStack() {
        // The stack grows 'downwards' in memory, the bottom (max address) is constant; save it.
        // Estimate stack top
        uint64_t dummy_variable = 0;
        dummy_variable++;
        stackBottom = page_aligned_up((void*)&dummy_variable);
        // And bottom
        refreshStackTop();
        stack_id = memory_ranges().addStack(this);
    }

    inline void deregisterStack() {
        memory_ranges().removeStack(stack_id);
    }

    ~StackMemoryRange() { deregisterStack(); }
};

extern thread_local StackMemoryRange local_stackMemRanges;

#endif // __MEMORY_RANGES_H_INCLUDED_

