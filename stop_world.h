#ifndef __STOP_WORLD_H__
#define __STOP_WORLD_H__

#include "helper_defs.h"
#include "memory_ranges.h"

#include "fcntl.h"
#include <mutex>
#include <signal.h>
#include "sys/mman.h"
#include "pthread.h"
#include <ucontext.h>
#include <unistd.h>

#include "errno.h"
#include "stdio.h"

#define MAX_THREADS 100
#define MAX_DIRTY_PAGES 10000

static constexpr size_t PAGEMAP_BUFF_SIZE = 256;
struct pagemap_buff_t {
  uint64_t buff[PAGEMAP_BUFF_SIZE];  
  uintptr_t beg;
  size_t size;

  inline bool read(uintptr_t page_ind, bool& result) {
    if (beg && (page_ind - beg <= size)) {
      result = (buff[page_ind - beg] >> 55) & 1; // Read soft-dirty bit
      return true;
    } else return false;
  }

  inline void invalidate() {
    beg = 0;
  }
};

class StopTheWorld {
protected:
  std::atomic<uint32_t> threadCount;
  pthread_t threads[MAX_THREADS];
  std::mutex threadsLock;

  std::atomic<uint64_t> StopCount;
  std::atomic<uint32_t> StoppedThreads;

  int pagemapFd;
  off_t pagemapOff;

  static constexpr int SIG_STOP_WORLD = SIGUSR1;
  static constexpr int SIG_RESUME_WORLD = SIGUSR2;

  sigset_t suspend_wait_mask;
public:
  void sigsus();
  void addThread(pthread_t thd);
  void resetThreads();

  bool isPageDirty(uintptr_t page);

  void protectHeap();
  void stop();
  void resume();

  void invalidateBuffer();

  StopTheWorld();
};

StopTheWorld& stop_the_world();

extern "C" void add_thread(pthread_t thd);

#endif // __STOP_WORLD_H__
