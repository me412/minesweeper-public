#ifndef __LINKED_LIST_H_INCLUDED_
#define __LINKED_LIST_H_INCLUDED_

/** Lock-free linked list, supporting a single update paralell with any number of reads.
 *  Note: there can be only a single update across all linked lists.
 */

#include <atomic>
#include "helper_defs.h"
#include "ngc_pool.h"

template <class T>
class LinkedList {
    struct Element {
        T item;
        Element *next;

        inline Element(const Element& other): item(other.item), next(other.next) {}
        inline Element(const T& item, Element *next): item(item), next(next) {}
    };

    std::atomic<Element*> head;
    static NGCPool<Element> pool;
  public:
    class iterator {
        Element *el;

        inline iterator(): el(NULL) {}
        inline iterator(Element *el): el(el) {}
      public:
        inline bool operator==(const iterator& rhs) const { return el == rhs.el; }
        inline bool operator!=(const iterator& rhs) const { return el != rhs.el; }
        inline iterator& operator++();
        inline iterator operator++(int ignored);
        inline T& operator*();
        inline static iterator begin(const LinkedList& list);
        inline static iterator end(const LinkedList& list) { return iterator(); }
    };
  private:
    friend class iterator;
  public:
    inline iterator begin() { return iterator::begin(*this); }
    inline iterator end() { return iterator::end(*this); }

    inline LinkedList(): head(NULL) {}

    inline bool empty() {
        return (head.load(std::memory_order_relaxed) == NULL);
    }

    // Insert new item to front. 
    // Note: no parallel updates, but there can be other threads iterating
    inline void insert(const T& x) {
        Element& elem = pool.alloc(Element(x, head.load(std::memory_order_relaxed)));
        head.store(&elem, std::memory_order_release);
    }

    void eraseAll() {
        for (Element* el = head.load(std::memory_order_acquire); el;) {
            Element* to_delete = el;
            el = el->next;

            pool.free(*to_delete);
        }

        head.store(NULL, std::memory_order_relaxed);
    }
};

template <class T>
NGCPool<typename LinkedList<T>::Element> LinkedList<T>::pool;

template <class T>
inline typename LinkedList<T>::iterator& LinkedList<T>::iterator::operator++() { el = el->next; }

template <class T>
inline typename LinkedList<T>::iterator LinkedList<T>::iterator::operator++(int ignored) {
    iterator result(el);
    el = el->next;
    return result;
}

template <class T>
inline T& LinkedList<T>::iterator::operator*() { return el->item; }

template <class T>
inline typename LinkedList<T>::iterator LinkedList<T>::iterator::begin(const LinkedList& list) {
    return iterator(list.head.load(std::memory_order_acquire));
}

#endif // __LINKED_LIST_H_INCLUDED_

