#include "minesweeper.h"

#include "central_quarantine.h"
#include "intercept.h"
#include "malloc.h"
#include "memory_ranges.h"
#include "quarantine.h"

void minesweeper_init() {
    static bool initialised = false;
    if (initialised) return;
    initialised = true;

    // Load malloc library and intercept free() 
    _init_intercept();
    // Connect to the malloc library and set it up.
    _init_malloc();
}

// Call minesweeper_init() when instantiated. This is for correct ordering below...
struct minesweeper_init_caller { minesweeper_init_caller() { minesweeper_init(); } };

// Put globals here for correct initialisation order
minesweeper_init_caller caller; // call minesweeper init after the previous two initialisations are done
CentralQuarantine central_quarantine;

