#ifndef __MINESWEEPER_CONF_H_
#define __MINESWEEPER_CONF_H_

namespace minesweeper {
		constexpr size_t MB = 1024 * 1024;

    constexpr bool STOP_WORLD_RESWEEP = false;

    constexpr bool CONCURRENT = true;
    constexpr size_t NUM_SWEEPERS = 6;
    constexpr size_t PARALLEL_DEALLOCATE_THRESHOLD = 600000;
//    constexpr size_t PARALLEL_MIN_HEAP_SIZE = 1 * MB;

    constexpr size_t NEWLY_INSERTED_THRESHOLD = 1 * MB;

    // Sweep policy
    // Sweep if at least 25% of all allocated memory is in the quarantine (except failed_frees)
    constexpr int SWEEP_THRESHOLD = 15;
		// Halt mover if at least 50% of all allocated memory (+HARD_CAP) is in quarantine. 
		constexpr int BACKSTOP_THRESHOLD = 70;
    // Sweep if total unmapped memory size is 900% of allocated memory
    constexpr int UNMAPPED_THRESHOLD = 900;
};

#endif

