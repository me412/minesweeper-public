#ifndef __SWEEP_THREAD_H_INCLUDED_
#define __SWEEP_THREAD_H_INCLUDED_

#include <atomic>
#include <condition_variable>
#include "helper_defs.h"
#include "immintrin.h"
#include "minesweeper_config.h"
#include "memory_ranges.h"
#include "quarantine.h"
#include "stop_world.h"
#include <thread>

class CentralQuarantine : private BaseQuarantine {
    ShadowSpace shadow;

    // Used to de-duplicate double-frees. When we release an allocation to the allocator,
    // we mark it, the second time it is not released.
    ShadowSpace recyclingShadow;

    std::mutex items_mtx; // Protects items, quarantined_mem and failed_frees
    ngc_vector<Item> new_items;
    size_t quarantined_mem;
    size_t failed_frees, last_failed_frees;
    size_t unmapped_mem;
    size_t unmapped_failed_frees, last_unmapped_failed_frees;

    ngc_vector<Item> items;

    #ifdef MSWEEPER_AVX512
        const static size_t buff_size = 4096;
        void* pointer_buffer[buff_size];
        void** ptr_buff = pointer_buffer;
    #endif

    void deallocateUnmarked(size_t start, size_t end);

    bool terminating;

    // Sweeper thread
    std::thread sweeper;
    std::mutex sweeper_mtx;
    std::condition_variable sweeper_cond;
    std::atomic_bool need_sweep_signalled;
    void sweep_thread_main();

    // Sweep helper threads
    struct alignas(64) sweep_helper_t {
        std::condition_variable cond;
        std::mutex mtx;
        uintptr_t start;
        uintptr_t end;
        bool deallocating;
        bool use_locking;
        int id;

        void main(CentralQuarantine* parent);
        void do_sweep(CentralQuarantine* parent, void* start_addr, void* end_addr, bool locking);
        void signal(void *start, void *end, bool locking = false);
        void signal(uint64_t start, uint64_t end);
        void signalReady(CentralQuarantine *parent);
    };
    sweep_helper_t helper_ctl[minesweeper::NUM_SWEEPERS];
    std::thread helpers[minesweeper::NUM_SWEEPERS];

    size_t released_mem;
    size_t released_unmapped;

    std::atomic_uint helpers_remaining;
    std::mutex helper_done_mtx;
    std::condition_variable helper_done_cond;

    std::condition_variable backstop_cond;

  public:
    CentralQuarantine() {
        if (!minesweeper::CONCURRENT) {
            ASSERT(!minesweeper::NUM_SWEEPERS);
            ASSERT(!minesweeper::STOP_WORLD_RESWEEP);
            registerMapRanges();
            return;
        }

        memory_ranges().launching_sweeper = true;
        sweeper = std::thread(&CentralQuarantine::sweep_thread_main, this);
        for (size_t i = 0; i < minesweeper::NUM_SWEEPERS; i++) {
            helper_ctl[i].id = i+1;
            helpers[i] = std::thread(&CentralQuarantine::sweep_helper_t::main,
                                          &helper_ctl[i], this);
        }

        // Remove sweeper threads and register main thread
        stop_the_world().resetThreads();
        stop_the_world().addThread(pthread_self());
        memory_ranges().launching_sweeper = false;
    }
    ~CentralQuarantine() {
        if (!minesweeper::CONCURRENT)
            return;

        // Terminate sweeper
        {WITH_LOCK(sweeper_mtx)
            terminating = true;
            sweeper_cond.notify_one();
        }
        for (size_t i = 0; i < minesweeper::NUM_SWEEPERS; i++) {
            {WITH_LOCK(helper_ctl[i].mtx)}
            helper_ctl[i].cond.notify_one();
        }

        for (size_t i = 0; i < minesweeper::NUM_SWEEPERS; i++)
            helpers[i].join();

        {WITH_LOCK(helper_done_mtx)
            helper_done_cond.notify_one();
        }

        sweeper.join();
    }

    void sweep();

    void signalSweep();

    // Move all items from the given vector into the central quarantine using this thread.
    // Block if hard cap hit on quarantine size.
    void moveAllFrom(ngc_vector<Item>& vect);

    // Do sampling-based sanity checking on the current elements (after sweep)
    void verifySweeps();

    // Mark items whose range contains ptr
    inline void markItemsContaining(void *ptr, void *min_entry, void *max_entry);
};

extern CentralQuarantine central_quarantine;

/* INLINE DEFINITIONS */

inline void CentralQuarantine::markItemsContaining(void *ptr, void *min_entry, void *max_entry) {
    // Only mark if within range of ptrs in quarantine
    if ((ptr >= min_entry) && (ptr < max_entry))
        shadow.mark(ptr);
}

#endif // __SWEEP_THREAD_H_INCLUDED_

