IDIR=.
CC=g++
MKDIR_P=mkdir -p

CFLAGS= -I$(IDIR) -fpic -std=c++17 -march=native

ifdef LDIR
CFLAGS+= -DLIB_DIR="\"$(LDIR)\"" -L$(LDIR)
else
  LDIR="."
endif

ifdef EXTRA_FLAGS
CFLAGS+= $(EXTRA_FLAGS)
endif

ODIR=obj

LIBS=-ljemalloc -ldl

_DEPS = central_quarantine.h helper_defs.h intercept.h linked_list.h malloc.h minesweeper.h minesweeper_config.h memory_ranges.h ngc_pool.h ngc_steady_vector.h ngc_vector.h proc_maps_parse.h quarantine.h shadow.h stop_world.h jemalloc/jemalloc.h jemalloc/jemalloc_typedefs.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = central_quarantine.o intercept.o je_malloc.o minesweeper.o memory_ranges.o proc_maps_parse.o quarantine.o stop_world.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))
OBJ_TGT = $(ODIR) $(OBJ)

all: opt
debug: CFLAGS += -DDEBUG -g
debug: minesweeper
opt: CFLAGS += -O3
opt: minesweeper
opt_sym: CFLAGS += -O3 -g3
opt_sym: minesweeper
opt_dbg_chk: CFLAGS += -DDEBUG -O3 -g3
opt_dbg_chk: minesweeper

$(ODIR):
	$(MKDIR_P) $(ODIR)

$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(ODIR)/%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

compile: ${OBJ_TGT}
	$(CC) -shared -o minesweeper.so $(OBJ) $(CFLAGS) $(LIBS)

minesweeper: compile
	cp minesweeper.so libminesweeper.so
	cp minesweeper.so $(LDIR)/libminesweeper.so

.PHONY: clean

clean:
	rm -f obj/*.o *~ minesweeper $(INCDIR)/*~
	rm minesweeper.so
