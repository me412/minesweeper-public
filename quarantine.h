#ifndef __QUARANTINE_H_INCLUDED_
#define __QUARANTINE_H_INCLUDED_

#include <atomic>
#include <cstring>
#include "helper_defs.h"
#include "immintrin.h"
#include "malloc.h"
#include "memory_ranges.h"
#include "minesweeper_config.h"
#include "ngc_vector.h"
#include <optional>
#include "shadow.h"

#ifdef __cplusplus
    #include <vector>

    extern "C" {
#endif //C++

/**
 * Put allocation into quarantine. Must be an allocation of the given size
 * that the programmer has called free() on.
 */
void quarantine_insert(void *ptr, size_t alloc_size);

#ifdef __cplusplus
    } // extern "C"

class BaseQuarantine {
  public:
    struct Item { // Quarantined item
        void *start_ptr; 
        void *end_ptr;

        inline bool contains(void *ptr) const
            { return (start_ptr <= ptr) && (ptr < end_ptr); }
        size_t size() const { return ((uintptr_t)end_ptr) - ((uintptr_t)start_ptr); }
        inline bool operator<(const Item& other) const { return end_ptr < other.end_ptr; }
        Item(void *ptr, size_t size): start_ptr(ptr), end_ptr(add_bytes(ptr,size)) {
            ASSERT(start_ptr <= end_ptr);
        }
        Item(void *s_ptr, void *e_ptr): start_ptr(s_ptr), end_ptr(e_ptr) {
            ASSERT(start_ptr <= end_ptr);
        }
        Item(): start_ptr(0), end_ptr(0) {}

        inline size_t unmap_and_zero() const;
        inline size_t remap() const;
        inline std::optional<Item> getUnmappedPortion() const;
    };
    // Compare function for ordering
    friend bool end_cmp(const void* p, const Item& it);
};

class Quarantine : public BaseQuarantine {
    ngc_vector<Item> _items;

    inline ngc_vector<Item>& items() { return _items; }

    size_t newly_inserted = 0;

  public:
    std::atomic<bool> initialised;
    Quarantine() : _items(16) {
        local_stackMemRanges.registerStack();

        // Last initialisation operation: mark as initialised and release non-atomic variables
        initialised.store(true, std::memory_order_release);
    }

    void insert(void *ptr, size_t size); // Returns true on success
};

extern thread_local Quarantine local_quarantine;

/* INLINE DEFINITIONS */
inline std::optional<BaseQuarantine::Item> BaseQuarantine::Item::getUnmappedPortion() const {
    void *unmap_start = page_aligned_up(start_ptr);
    void *unmap_end = page_aligned(end_ptr);
    if (unmap_start < unmap_end)
        return std::optional<Item>(std::in_place, unmap_start, unmap_end);
    return std::nullopt;
}

inline size_t BaseQuarantine::Item::unmap_and_zero() const {
    std::optional<Item> unmapped_opt = getUnmappedPortion();
    if (unmapped_opt) {
        const Item& unmapped_part = (const Item&)unmapped_opt;
        unmap_mem_range(unmapped_part.start_ptr, unmapped_part.size());
        if (start_ptr < unmapped_part.start_ptr)
            memset(start_ptr, 0, size_bytes(start_ptr, unmapped_part.start_ptr));
        if (unmapped_part.end_ptr < end_ptr)
            memset(unmapped_part.end_ptr, 0,
                   size_bytes(unmapped_part.end_ptr, end_ptr));

        return unmapped_part.size();
    } else {
        // Zero, so that pointers here do not interfere with liveness
        memset(start_ptr, 0, size());
        return 0;
    }
}

inline size_t BaseQuarantine::Item::remap() const {
    std::optional<Item> unmapped_opt = getUnmappedPortion();
    if (unmapped_opt) {
        const Item& unmapped_part = (const Item&)unmapped_opt;
        remap_mem_range(unmapped_part.start_ptr, unmapped_part.size());
        return unmapped_part.size();
    }
    return 0;
}

#endif // C++
#endif // __QUARANTINE_H_INCLUDED_

