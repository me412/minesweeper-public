#ifndef __NGC_POOL_INCLUDED_H_
#define __NGC_POOL_INCLUDED_H_

// Pool allocator backed by non-garbage-collected storage. (Not thread-safe)

#include "ngc_steady_vector.h"

template <class T>
class NGCPool {
    union ObjectOrFreeList {
        ObjectOrFreeList *nextFree;
        T object;

        ObjectOrFreeList(const T& item): object(item) {}
        ObjectOrFreeList(const ObjectOrFreeList* ptr): nextFree(ptr) {}
    };

    ngc_steady_vector<ObjectOrFreeList> store;
    ObjectOrFreeList *firstFree = NULL;

  public:
    inline void free(T& obj) {
        // Call destructor
        obj.~T();

        // Register in free list
        ObjectOrFreeList& item = (ObjectOrFreeList&)obj;
        item.nextFree = firstFree;
        firstFree = &item;
    }

    inline T& alloc(const T& obj) {
        if (firstFree) {
            // Allocate first item of free list
            ObjectOrFreeList &result = *firstFree;
            firstFree = result.nextFree;
            result.object = T(obj);
            return result.object;
        } else {
            // Expand treeNodes vector
            store.emplace_back(obj);
            return store.last().object;
        }
    }
};

#endif // __NGC_POOL_INCLUDED_H_
